Data from *Assessing the impact of incomplete species sampling on estimates of speciation and extinction rates* by Rachel CM Warnock, Tracy A Heath, Tanja Stadler

Contact: rachel.warnock@bsse.ethz.ch

##Software

Trees and fossils were simulated using the folowing R packages:

* TreeSim v2.3 (as of the time of writing this is the latest version of this package)
* FossilSim v1.0 (SHA 3933b878ffe6e719c5c273cef36e1b27352e274b)

Speciation and extinction rates were calculated using the following programs:

* DPPDiv v2.0 (SHA 1d5a2f5bc8dd6251298c805845b24317db675c53)
* the fbdR R package v1.0.1 (SHA f917635c8abb9ffa4a56c78603d353a130d12eaa)

Analyses was performed using R version 3.5.1.

The R packages can be installed using the following commands:

	 library(devtools)
    install.packages("TreeSim")
    install_github("fossilsim/fossilsim@v1.0")
    install_github("rachelwarnock/fbdr@v1.0.1")

##Input file guide

###Prefixes

* **02\_FBD**: analysis under the FBD process including extant singletons 
* **04\_FBD**: analysis under the FBD process with 1/0 sampling including extant singletons (fossil ages = midpoint ages, ignoring true species durations)
* **05\_FBD**: analysis under the FBD process excluding extant singletons 
* **06\_FBD**: analysis under the FBD process with 1/0 sampling excluding extant singletons (fossil ages = midpoint ages, ignoring true species durations)
* **07\_FBD**: analysis under the FBD process with 1/0 sampling excluding extant singletons (fossil ages = midpoint ages, respecting true species durations)
* **05\_BD**: analysis under the BD process
* **06\_BD**: analysis under the BD process with 1/0 sampling
* **ranges**: analysis using the boundary-crosser, boundary-crosser + singletons and the three-timer methods
* **useful**: code used to calculate the proportion of sampled taxa used by each method

###Suffixes

* **\_04**: turnover = 0.1, per interval sampling probability = 0.1
* **\_05**: turnover = 0.1, per interval sampling probability = 0.5
* **\_06**: turnover = 0.1, per interval sampling probability = 0.99
* **\_07**: turnover = 0.5, per interval sampling probability = 0.1
* **\_08**: turnover = 0.5, per interval sampling probability = 0.5
* **\_09**: turnover = 0.5, per interval sampling probability = 0.99
* **\_10**: turnover = 0.9, per interval sampling probability = 0.1
* **\_11**: turnover = 0.9, per interval sampling probability = 0.5
* **\_12**: turnover = 0.9, per interval sampling probability = 0.99
 
* **\_13**: turnover = 0.5, non-uniform sampling (0.01 to 0.99)
* **\_14**: turnover = 0.5, non-uniform sampling (0.99 to 0.01)
