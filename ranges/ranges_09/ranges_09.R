
set.seed(1234)

reps = 100
tips = 100
lambda = 1
mu = 0.5

strata = 20
sampling = 0.99

###### 

trees<-TreeSim::sim.bd.taxa(tips,reps,lambda,mu)

fossils<-lapply(trees, function(x) {
  y<-FossilSim::basin.age(x)
  FossilSim::sim.fossils.unif(x,y,strata,sampling)
  })

analysis<-function(trees,fossils){
  
  sym.un.b = c()
  sym.un.d = c()
  sym.bc.b = c()
  sym.bc.d = c()
  sym.tt.b = c()
  sym.tt.d = c()
  sym.gf.b = c()
  sym.gf.d = c()
  
  asym.un.b = c()
  asym.un.d = c()
  asym.bc.b = c()
  asym.bc.d = c()
  asym.tt.b = c()
  asym.tt.d = c()
  asym.gf.b = c()
  asym.gf.d = c()
  
  sym.tt.s = c()
  asym.tt.s = c()
  
  for (i in 1:reps){
    
    tree1<-trees[[i]]
    f1<-fossils[[i]]
    f2<-FossilSim::add.extant.occ(tree1,f1)
    
    ba<-FossilSim::basin.age(tree1)
    
    un = fbdR::uncorrected.rates(f2,ba,20)
    bc = fbdR::boundary.crosser.rates(f2,ba,20)
    tt = fbdR::three.timer.rates(f2,ba,20)  
    gf = fbdR::gap.filler.rates(f2,ba,20)
    
    sym.un.b = c(sym.un.b, un$speciation)
    sym.un.d = c(sym.un.d, un$extinction)
    sym.bc.b = c(sym.bc.b, bc$speciation)
    sym.bc.d = c(sym.bc.d, bc$extinction)
    sym.tt.b = c(sym.tt.b, tt$speciation)
    sym.tt.d = c(sym.tt.d, tt$extinction)
    sym.gf.b = c(sym.gf.b, gf$speciation)
    sym.gf.d = c(sym.gf.d, gf$extinction)
    
    sym.tt.s = c(sym.tt.s, tt$sampling)
    
    f3 = FossilSim::asymmetric.fossil.mapping(tree1,f2)
    
    un = fbdR::uncorrected.rates(f3,ba,20)
    bc = fbdR::boundary.crosser.rates(f3,ba,20)
    tt = fbdR::three.timer.rates(f3,ba,20)  
    gf = fbdR::gap.filler.rates(f3,ba,20)
    
    asym.un.b = c(asym.un.b, un$speciation)
    asym.un.d = c(asym.un.d, un$extinction)
    asym.bc.b = c(asym.bc.b, bc$speciation)
    asym.bc.d = c(asym.bc.d, bc$extinction)
    asym.tt.b = c(asym.tt.b, tt$speciation)
    asym.tt.d = c(asym.tt.d, tt$extinction)
    asym.gf.b = c(asym.gf.b, gf$speciation)
    asym.gf.d = c(asym.gf.d, gf$extinction)
    
    asym.tt.s = c(asym.tt.s, tt$sampling)
    
  }
  
  write.table(sym.un.b,col.names = "birth",row.names = F,file="sym.un.b.txt")
  write.table(sym.un.d,col.names = "death",row.names = F,file="sym.un.d.txt")
  write.table(sym.bc.b,col.names = "birth",row.names = F,file="sym.bc.b.txt")
  write.table(sym.bc.d,col.names = "death",row.names = F,file="sym.bc.d.txt")
  write.table(sym.tt.b,col.names = "birth",row.names = F,file="sym.tt.b.txt")
  write.table(sym.tt.d,col.names = "death",row.names = F,file="sym.tt.d.txt")
  write.table(sym.gf.b,col.names = "birth",row.names = F,file="sym.gf.b.txt")
  write.table(sym.gf.d,col.names = "death",row.names = F,file="sym.gf.d.txt")
  
  write.table(asym.un.b,col.names = "birth",row.names = F,file="asym.un.b.txt")
  write.table(asym.un.d,col.names = "death",row.names = F,file="asym.un.d.txt")
  write.table(asym.bc.b,col.names = "birth",row.names = F,file="asym.bc.b.txt")
  write.table(asym.bc.d,col.names = "death",row.names = F,file="asym.bc.d.txt")
  write.table(asym.tt.b,col.names = "birth",row.names = F,file="asym.tt.b.txt")
  write.table(asym.tt.d,col.names = "death",row.names = F,file="asym.tt.d.txt")
  write.table(asym.gf.b,col.names = "birth",row.names = F,file="asym.gf.b.txt")
  write.table(asym.gf.d,col.names = "death",row.names = F,file="asym.gf.d.txt")
  
  write.table(sym.tt.s,col.names = "sampling",row.names = F,file="sym.tt.s.txt")
  write.table(asym.tt.s,col.names = "sampling",row.names = F,file="asym.tt.s.txt")

  #read.table("asym.gf.d.txt",header=T)
  
  #EOF
}

analysis(trees,fossils)



