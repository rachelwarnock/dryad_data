
set.seed(1234)

reps = 100
tips = 100
lambda = 1
mu = 0.9

strata = 20
sampling = 0.99

###### 

trees<-TreeSim::sim.bd.taxa(tips,reps,lambda,mu)

fossils<-lapply(trees, function(x) {
  y<-FossilSim::basin.age(x)
  FossilSim::sim.fossils.unif(x,y,strata,sampling)
  })

### info to collect

FBD.t.asym.rho1 = c()

FBD.t.asym.rho0 = c()

uncorrected.t.asym = c()

boundary.crosser.t.asym = c()

three.timer.t.asym = c()

for (i in 1:reps){
  
  tree1 = trees[[i]]
  
  f1 = fossils[[i]]
  f2 = FossilSim::add.extant.occ(tree1,f1)
  ba = FossilSim::basin.age(tree1)
  
  ### asymmetric speciation
  
  ### total number of species (asymmetric)
  #out = FossilSim::asymmetric.ages(tree1)
  #total = length(out$sp)
  
  ### total number of extinct species (asymmetric)
  #tol = 1e-8
  #extinct = total - length(which(out$end < tol))
  
  f3 = FossilSim::asymmetric.fossil.mapping(tree1,f2)
  f4 = FossilSim::asymmetric.fossil.mapping(tree1,f1) # note may also contain extant taxa

  total = length(unique(f3$sp))
  
  ### FBD
  FBD.t.asym.rho1 = c(FBD.t.asym.rho1, length(unique(f3$sp))/total)

  ### FBD & BD
  FBD.t.asym.rho0 = c(FBD.t.asym.rho0, length(unique(f4$sp))/total)
  
  ### uncorrected
  out = fbdR::interval.types.bc(f3,ba,20,return.useful = T)
  un = unique(out$useful.un)
  
  uncorrected.t.asym = c(uncorrected.t.asym, length(un)/total)

  ## boundary crosser
  bc = unique(out$useful.bc)
  
  boundary.crosser.t.asym = c(boundary.crosser.t.asym, length(bc)/total)
  
  ## three timer
  out = fbdR::interval.types.3t(f3,ba,20,return.useful = T)  
  tt = unique(out)
  
  three.timer.t.asym = c(three.timer.t.asym, length(tt)/total)
  
}  
  
df = data.frame(FBD.t.asym.rho0 = FBD.t.asym.rho0,
                FBD.t.asym.rho1 = FBD.t.asym.rho1,
                uncorrected.t.asym = uncorrected.t.asym,
                boundary.crosser.t.asym = boundary.crosser.t.asym,
                three.timer.t.asym = three.timer.t.asym)

write.table(df, row.names = FALSE, quote = FALSE, file = "useful.txt")


# mean(FBD.t)
# mean(FBD.t.asym)
#   
# mean(uncorrected.t)
# mean(uncorrected.t.asym)
#   
# mean(boundary.crosser.t)
# mean(boundary.crosser.t.asym)
#   
# mean(three.timer.t)
# mean(three.timer.t.asym)

