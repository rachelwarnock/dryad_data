for i in `seq 1 100`
do
echo dppdiv=~/FDPPDIV/src/dppdiv-seq > dp_$i.sh
echo \$dppdiv -frofbd -n 200000 -sf 20 -pf 20000 -bdp 3 -rho 0 -psiPrior 2 -divPrior 2 -bexpR 1 -dexpR 1 -pexpR 1 -cal 08_$i.cal -out 08_$i >> dp_$i.sh
done

echo dppdiv=~/FDPPDIV/src/dppdiv-seq > dp_prior_$i.sh
echo \$dppdiv -frofbd -n 200000 -sf 20 -pf 20000 -bdp 3 -rho 0 -psiPrior 2 -divPrior 2 -bexpR 1 -dexpR 1 -pexpR 1 -cal 08_$i.cal -out 08_prior_$i -rnp >> dp_prior_$i.sh
