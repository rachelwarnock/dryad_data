set.seed(1234)

reps = 100
tips = 100
lambda = 1
mu = 0.5

strata = 20
sampling = 0.1
prefix = "07_"

###### 

# simulate trees
trees<-TreeSim::sim.bd.taxa(tips,reps,lambda,mu)

# simulate fossils
fossils<-lapply(trees, function(x) {
  y<-FossilSim::basin.age(x)
  FossilSim::sim.fossils.unif(x,y,strata,sampling,convert.rate = F)
})

# generate input files for dppdiv
for (i in 1:reps){
  
  tree1<-trees[[i]]
  f1<-fossils[[i]]
  # use midpoint ages
  y<-FossilSim::basin.age(tree1)
  f1$h <- f1$h - (y/40)
  f2<-FossilSim::add.extant.occ(tree1,f1)
  f3<-FossilSim::asymmetric.fossil.mapping(tree1,f2)
  FossilParser::parse.dppdiv.FRG(f3,file=paste(prefix,i,".cal",sep=""))
  
}

origins<-sapply(trees, function(x) {
  max(TreeSim::getx(x, sersampling = 1)) + x$root.edge
})

write.table(origins,file="origins.txt")
