##### functions

# summarise results and assess convergence
assess.convergence = function(file){
  
  eff = function(acf) 1 / (1 + 2 * sum(acf$acf[-1]))
  
  line.number = as.numeric(system(paste0("wc -l ", file, " | awk {'print $1'}"), intern = T))
  
  if(line.number < 10) {
    cat("Insufficent mcmc samples (<10) ", file, " skipping convergence assessment\n", sep = "", file = "convergence.log", append = TRUE)
    return(NULL)
  } else if(line.number != 10002){
    cat("Insufficent mcmc samples ", file, "\n", sep = "", file = "convergence.log", append = TRUE)
    mcmc = read.table(file, header = TRUE, nrows = line.number-1)
  } else mcmc = read.table(file, header = TRUE)
  
  ## remove initial step
  mcmc = mcmc[-1,]
  ## remove burn in
  burnin = length(mcmc[[1]]) * 0.1
  mcmc = mcmc[-(0:burnin),]
  
  ## assess convergence
  parameters = c("FBD.lambda", "FBD.mu", "FBD.psi")
  
  for(j in parameters){
    p = mcmc[[j]]
    method1 = eff(acf(p, plot = F)) * length(p)
    method2 = coda::effectiveSize(p)[[1]]
    method3 = mcmcse::ess(p)
  }
  
  if(any(c(method1, method2, method3) < 200))
    cat("Potential issue with ", file, "\n", sep = "", file = "convergence.log", append = TRUE)
  
  return(mcmc)
}

# parse mcmc results
parse.mcmc.results = function(mcmc.prefix, suffix){
  
  for(i in 1:100){
    prefix = paste(mcmc.prefix, "_", suffix, "/", suffix,sep = "")
    file = paste(prefix, "_", i, ".FR.out", sep = "")
    
    # open mcmc file & assess convergence
    mcmc = assess.convergence(file)
    
    if(is.null(mcmc)) cat("Something went wrong with ", file, "\n", sep = "", file = "convergence.log", append = TRUE)
    
  }
  
}

parse.mcmc.results.all = function(mcmc.prefix){
  
  # out04 = parse.mcmc.results(mcmc.prefix, "04")
  # out05 = parse.mcmc.results(mcmc.prefix, "05")
  # out06 = parse.mcmc.results(mcmc.prefix, "06")
  # 
  # out07 = parse.mcmc.results(mcmc.prefix, "07")
  # out08 = parse.mcmc.results(mcmc.prefix, "08")
  # out09 = parse.mcmc.results(mcmc.prefix, "09")
  # 
  # out10 = parse.mcmc.results(mcmc.prefix, "10")
  # out11 = parse.mcmc.results(mcmc.prefix, "11")
  out12 = parse.mcmc.results(mcmc.prefix, "12")
  
  #eof
}

######

cat(NULL, file = "convergence.log", append = FALSE)

df1 = parse.mcmc.results.all("02_FBD") # 02_FBD: table 1 FBD rho = 1
df2 = parse.mcmc.results.all("05_FBD") # 05_FBD: table 3 FBD rho = 0
df3 = parse.mcmc.results.all("04_FBD") # 04_FBD: table 2 FBD 1/0 rho = 1
df4 = parse.mcmc.results.all("06_FBD") # 06_FBD: table 4 FBD 1/0 rho = 0

df5 = parse.mcmc.results.all("05_BD") # 05_BD: table 5 BD
df6 = parse.mcmc.results.all("06_BD") # 06_BD: table 6 BD 1/0
