##### use fossilsim v2 to convert between paleotree and fossilsim object
##### see below for an example using fossilsim v1

devtools::install_github("rachelwarnock/fbdR") # latest version v1.0.1
devtools::install_github("fossilsim/fossilsim") # latest version v2.0

set.seed(123)

reps = 100
tips = 100
lambda = 1
mu = 0.5

strata = 20
sampling = 0.5

trees = TreeSim::sim.bd.taxa(tips,reps,lambda,mu)

df1 = data.frame() # tree wide
df2 = data.frame() # average per interval

for (i in 1:reps){
  
  tree1 = trees[[i]]
  
  # tidy max based on tree age
  max = round(FossilSim::tree.max(tree1),1)+0.1
  
  # simulate fossils
  f1 = FossilSim::sim.fossils.intervals(tree = tree1, max.age = max, strata = strata, 
                                        probabilities = rep(sampling, strata) )
  f2 = FossilSim::sim.extant.samples(f1, tree = tree1, rho = 1)
  
  # assume budding speciation
  tx = FossilSim::sim.taxonomy(tree1, beta = 0)
  f3 = FossilSim::reconcile.fossils.taxonomy(f2, tx)
  
  # bin samples
  f4 = FossilSim::sim.interval.ages(f3, taxonomy = tx, max.age = max, strata = strata)
  
  bc = fbdR::boundary.crosser.rates(f4,max,strata,return.intervals=TRUE)
  tt = fbdR::three.timer.rates(f4,max,strata,return.intervals=TRUE)
  
  # tree wide
  df1 = rbind(df1, data.frame(bc.b = bc$speciation, 
                              bc.d = bc$extinction, 
                              tt.b = tt$speciation,
                              tt.d = tt$extinction,
                              tt.s = tt$sampling))
  
  # average interval
  df2 = rbind(df2, data.frame(bc.b = mean(bc$per.interval.rates$p,na.rm=TRUE), 
                              bc.d = mean(bc$per.interval.rates$q,na.rm=TRUE), 
                              tt.b = mean(tt$per.interval.rates$p,na.rm=TRUE),
                              tt.d = mean(tt$per.interval.rates$q[-1],na.rm=TRUE)))
  
}

#boundary crosser rates
mean(df1$bc.b, na.rm = TRUE)
mean(df1$bc.d, na.rm = TRUE)
mean(df2$bc.b, na.rm = TRUE)
mean(df2$bc.d, na.rm = TRUE)
#three timer rates
mean(df1$tt.b, na.rm = TRUE)
mean(df1$tt.d, na.rm = TRUE)
mean(df2$tt.b, na.rm = TRUE)
mean(df2$tt.d, na.rm = TRUE)

mean(df1$tt.s, na.rm = TRUE)

# paleotree example / not conditioning on tip number 

set.seed(1)

record <- paleotree::simFossilRecord( p = 0.1, 
                           q = 0.1, 
                           r = 0.1, 
                           nruns = 1,
                           nSamp = c(200,300) )

# in case of interest the thing that makes this function slow is paleotree::taxa2phylo(paleotree::fossilRecord2fossilTaxa(record))
f = FossilSim::paleotree.record.to.fossils(record)

tree1 = f$tree
f1 = f$fossils
tx = f$taxonomy

max = round(FossilSim::tree.max(tree1),1)+0.1
strata = 20

f4 = FossilSim::sim.interval.ages(f1, taxonomy = tx, max.age = max, strata = strata)

bc = fbdR::boundary.crosser.rates(f4,max,strata,return.intervals=TRUE)
tt = fbdR::three.timer.rates(f4,max,strata,return.intervals=TRUE)

# tree wide
bc.b = bc$speciation
bc.d = bc$extinction 
tt.b = tt$speciation
tt.d = tt$extinction
tt.s = tt$sampling

bc.b = mean(bc$per.interval.rates$p,na.rm=TRUE) 
bc.d = mean(bc$per.interval.rates$q,na.rm=TRUE) 
tt.b = mean(tt$per.interval.rates$p,na.rm=TRUE)
tt.d = mean(tt$per.interval.rates$q,na.rm=TRUE)


##### code used in the paper // fossilsim changed a lot between v1 and v2.

library(devtools) 
install_github("rachelwarnock/fbdR@2.0")
install_github("fossilsim/fossilsim@v1.0.1")

set.seed(123)

reps = 100
tips = 100
lambda = 1
mu = 0.5

strata = 20
sampling = 0.5

###### 

trees<-TreeSim::sim.bd.taxa(tips,reps,lambda,mu)

fossils<-lapply(trees, function(x) {
  y<-FossilSim::basin.age(x)
  FossilSim::sim.fossils.unif(x,y,strata,sampling)
})

df1 = data.frame() # tree wide
df2 = data.frame() # average per interval

for (i in 1:reps){
  
  tree1<-trees[[i]]
  f1<-fossils[[i]]
  f2<-FossilSim::add.extant.occ(tree1,f1)
  
  ba<-FossilSim::basin.age(tree1)
  
  f3 = FossilSim::asymmetric.fossil.mapping(tree1,f2)
  
  bc = fbdR::boundary.crosser.rates(f3,ba,20,return.intervals=TRUE)
  tt = fbdR::three.timer.rates(f3,ba,20,return.intervals=TRUE)
  
  # tree wide
  df1 = rbind(df1, data.frame(bc.b = bc$speciation, 
                              bc.d = bc$extinction, 
                              tt.b = tt$speciation,
                              tt.d = tt$extinction,
                              tt.s = tt$sampling))
  
  # average interval
  df2 = rbind(df2, data.frame(bc.b = mean(bc$per.interval.rates$p,na.rm=TRUE), 
                              bc.d = mean(bc$per.interval.rates$q,na.rm=TRUE), 
                              tt.b = mean(tt$per.interval.rates$p,na.rm=TRUE),
                              tt.d = mean(tt$per.interval.rates$q,na.rm=TRUE) ))
  
}

#boundary crosser rates
mean(df1$bc.b, na.rm = TRUE)
mean(df1$bc.d, na.rm = TRUE)
mean(df2$bc.b, na.rm = TRUE)
mean(df2$bc.d, na.rm = TRUE)
#three timer rates
mean(df1$tt.b, na.rm = TRUE)
mean(df1$tt.d, na.rm = TRUE)
mean(df2$tt.b, na.rm = TRUE)
mean(df2$tt.d, na.rm = TRUE)




